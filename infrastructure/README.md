# Terraform Deployment

This folder contains Terraform configuration files for deploying a Kubernetes cluster on Google Kubernetes Engine (GKE) using Terraform.

## Prerequisites

- Terraform: Make sure you have Terraform installed on your local machine.
- Google Cloud Platform (GCP) Account: You need a GCP account to create and manage the GKE cluster.
- Project and Credentials: Create a GCP project and set up the necessary credentials to authenticate Terraform with your GCP account.
- Install & Configure:
    - gcloud
    - kubectl
    - kustomize

- Enable Necessary API’s
    - Kubernetes Engine API
    - Compute engine API

## Files

- **gke.tf**: This file defines the Terraform configuration for creating the GKE cluster and related resources. Here's a breakdown of each block in the `gke.tf` file:

  - **terraform block**: Specifies the required provider for Terraform, in this case, the Google provider with version 4.67.0.
  
  - **module "gke_auth"**: Defines a Terraform module for GKE authentication. It sources the module from `terraform-google-modules/kubernetes-engine/google//modules/auth` with version 26.1.1. It depends on the `module.gke` block.
  
  - **resource "local_file" "kubeconfig"**: Declares a local file resource to generate the `kubeconfig` file using the `module.gke_auth.kubeconfig_raw` output. The file will be created in the same directory as the `gke.tf` file.
  
  - **module "gcp-network"**: Specifies a module for creating a Google Cloud Platform (GCP) network using the `terraform-google-modules/network/google` module with version 7.0.0. It sets various configurations such as project ID, network name, subnets, and secondary IP ranges.
  
  - **data "google_client_config" "default"**: Retrieves the default Google Cloud client configuration, including the access token, to be used by the Kubernetes provider.
  
  - **provider "kubernetes"**: Configures the Kubernetes provider with the necessary information, such as the GKE cluster's endpoint, access token, and cluster CA certificate.
  
  - **module "gke"**: Declares a module for creating a private GKE cluster using the `terraform-google-modules/kubernetes-engine/google//modules/private-cluster` module with version 26.1.1. It sets configurations for the project ID, cluster name, region, network, subnetwork, IP ranges, and node pool specifications.

- **terraform.tfvars**: This file contains the variable values used in the Terraform configuration.

## Usage

1. Ensure that you have Terraform installed on your local machine.
2. Update the values in the `terraform.tfvars` file according to your desired configuration.
3. Run `terraform init` to initialize the Terraform configuration.
4. Run `terraform plan` to review the execution plan.
5. Run `terraform apply` to create the GKE cluster and associated resources.
6. After successful deployment, the Kubernetes configuration file (`kubeconfig`) will be generated in the same directory.
7. Configure the kubectl command-line tool to connect to the specified cluster and set the context for subsequent commands: 
`gcloud container clusters get-credentials <cluster-name> --zone <zone> --project <project-id>`
8. Replace <cluster-name> with the name of your GKE cluster, <zone> with the zone where the cluster is located, and <project-id> with your Google Cloud project ID.

## Configuration

The `terraform.tfvars` file contains the following variables:

- **project_id**: The ID of the project where the GKE cluster will be created.
- **cluster_name**: The name of the GKE cluster.
- **env_name**: The environment name for the GKE cluster.
- **region**: The region where the GKE cluster will be provisioned.
- **network**: The name of the VPC network to be created for the GKE cluster.
- **subnetwork**: The name of the subnet to be created for the GKE cluster.
- **ip_range_pods_name**: The secondary IP range for pods.
- **ip_range_services_name**: The secondary IP range for services.

## References

- [Terraform Google Modules](https://registry.terraform.io/namespaces/terraform-google-modules): A collection of Terraform modules provided by Google for managing Google Cloud resources.
- [Terraform Google GKE Auth Module](https://registry.terraform.io/modules/terraform-google-modules/kubernetes-engine/google/latest/submodules/auth): Terraform module for setting up GKE authentication.
- [Terraform Google GKE Private Cluster Module](https://registry.terraform.io/modules/terraform-google-modules/kubernetes-engine/google/latest/submodules/private-cluster): Terraform module for creating a private GKE cluster.

**Note**: The `.tfvars` file is included in this repository for demonstration purposes.  In a real-world scenario, it is recommended to exclude sensitive information, such as API keys or passwords, from version control systems.