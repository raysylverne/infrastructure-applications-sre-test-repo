Sure! Here's an updated version of the README that incorporates the suggested changes without significantly increasing the length:

# Deploying Podinfo Application using Kustomize on GKE

This guide explains how to deploy the Podinfo application to a Google Kubernetes Engine (GKE) cluster using Kustomize.

## Introduction

This guide provides step-by-step instructions for deploying the Podinfo application on a GKE cluster using Kustomize. It assumes you have a GKE cluster already set up and have the necessary tools (like `kubectl`) installed and configured.

## Steps

1. Create a `kustomization.yaml` file with the following content:

   ```yaml
   apiVersion: kustomize.config.k8s.io/v1beta1
   kind: Kustomization

   namespace: default

   resources:
   - github.com/stefanprodan/podinfo/kustomize
   - loadbalancer-service.yaml
   - ingress-resource.yaml

   patches:
   - path: deployment-patch.yaml
   ```

   This file defines the resources to deploy, including the Podinfo application, LoadBalancer service, Ingress resource, and the deployment patch.

2. Create a `loadbalancer-service.yaml` file and define a LoadBalancer service to expose the Podinfo application on port 80.

3. Create an `ingress-resource.yaml` file and define an Ingress resource to route traffic to the Podinfo application.

4. Apply the deployment using Kustomize:
   ```
   kubectl apply -k . or <directory where configuration files are located>
   kubectl apply -k testing
   ```

   This command deploys the Podinfo application and associated resources to your GKE cluster. It will create the necessary Deployment, LoadBalancer service, and Ingress resource.

5. Monitor the deployment:
   ```
   kubectl get all
   ```

   Use this command to check the status of the deployed resources. Ensure that the Podinfo deployment, LoadBalancer service, and Ingress resource are all in the "Running" state.
![Resources](https://gitlab.com/raysylverne/infrastructure-applications-sre-test-repo/-/raw/e96529fec6c00452c2e8e49271b1f8d87c603477/Kustomize_Test_Files/images/Resources.png)

6. Access the Podinfo application:
   ```
   kubectl get ingress
   ```

   Retrieve the external IP address associated with the Ingress resource. Once the IP address is available, you can access the Podinfo application using the following URL: `http://<EXTERNAL-IP>/`.

You can now access the application by visiting the provided URL.
![Podinfo Image](https://gitlab.com/raysylverne/infrastructure-applications-sre-test-repo/-/raw/main/Kustomize_Test_Files/images/Podinfo.png)

## References

- [Kustomize Feature List](https://kubernetes.io/docs/tasks/manage-kubernetes-objects/kustomization/#kustomize-feature-list)
- [Introduction to Kustomize](https://kubectl.docs.kubernetes.io/guides/introduction/kustomize/)
- [Kubernetes Kustomize Tutorial](https://antonputra.com/kubernetes/kubernetes-kustomize-tutorial/)
- [KodeKloud Kubernetes](https://kodekloud.com/topic/kubernetes-on-aws-eks/)